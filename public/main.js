const navBar = document.querySelector(".header-navigation");
const btn = document.querySelector(".header-btn");
const toggleClasses = function () {
  navBar.classList.toggle("opened");
  document.body.classList.toggle("body-scroll-off");
  btn.classList.toggle("opened");
};

const navLinks = Array.from(
  document.querySelectorAll(".header-navigation__link")
);

btn.addEventListener("click", toggleClasses);
navLinks.forEach(function (item) {
  item.addEventListener("click", toggleClasses);
});

//   scrolling script start

Array.from(document.querySelectorAll(".header-navigation__link")).forEach(
  (link) => {
    link.addEventListener("click", (event) => {
      event.preventDefault();

      return window.scrollTo({
        top: document.querySelector(link.getAttribute("href")).offsetTop -88,
        behavior: "smooth",
      });
    });
  }
);

const homeBtn = document.querySelector("#homeBtn");
homeBtn.addEventListener("click", (event) => {
  event.preventDefault();

  return window.scrollTo({
    top: 0,
    behavior: "smooth",
  });
});

// burger BTN

// const burgerBtn = document.querySelector(".burger-btn");
// const toggleBtn = () => {
//   burgerBtn.classList.toggle("burger-btn_active");
// };
// burgerBtn.addEventListener("click", toggleBtn);
